<?php 
	require 'ape.php';
	require 'frog.php';

	$sheep = new Hewan("Shaun");
	echo "Nama : ".$sheep->nama."<br>";
	echo "Kaki :".$sheep->kaki."<br>";
	echo "Darah Dingin : ".$sheep->darahdingin."<br>";

	echo "<br>";

	$hewan1= new Ape("Sun Gokong");

	echo "Nama : ".$hewan1->nama."<br>";
	echo "Kaki :".$hewan1->kaki."<br>";
	echo "Darah Dingin : ".$hewan1->darahdingin."<br>";
	echo "Suara : ".$hewan1->suara."<br>";

	echo "<br>";

	$hewan2= new Frog("Bangkong");

	echo "Nama : ".$hewan2->nama."<br>";
	echo "Kaki :".$hewan2->kaki."<br>";
	echo "Darah Dingin : ".$hewan2->darahdingin."<br>";
	echo "Suara : ".$hewan2->suara."<br>";
?>